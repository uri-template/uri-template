;; Copyright 2007-2012 Vladimir Sedach <vas@oneofus.la>

;; SPDX-License-Identifier: LGPL-3.0-or-later

;; This file is part of uri-template.

;; uri-template is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; uri-template is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with uri-template. If not, see
;; <https://www.gnu.org/licenses/>.

(in-package #:uri-template)

(parenscript:defpsmacro maybe-uri-encode (x)
  (if uri-encode? `(encode-u-r-i-component ,x) x))

(parenscript:defpsmacro uri-template (&rest template-args)
  `(+ ,@template-args))
